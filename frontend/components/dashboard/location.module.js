import mapStyle from "./Map.module.scss";
import {MapContainer, TileLayer} from "react-leaflet";

export default function LocationModule() {
    return (
        <div className={mapStyle.map}>
            <MapContainer center={[55.115305, 37.309144]} zoom={10} scrollWheelZoom={false}
                          style={{height: "100%", width: "100%", borderRadius: "24px"}}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
            </MapContainer>
        </div>
    )
}
