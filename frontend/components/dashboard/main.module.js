import style from "./Main.module.scss";
import TopBarModule from "./topbar.module";

import dynamic from "next/dynamic"
import WrapperLayoutModule from "./wrapper.layout.module";

const MapModule = dynamic(() => import("./map.module"), { ssr:false })

export default function MainModule( {geo} ) {
    return (
        <div className={style.main}>
            <TopBarModule/>
            <WrapperLayoutModule>
                <MapModule geo={geo}/>
            </WrapperLayoutModule>
        </div>
    )
}
