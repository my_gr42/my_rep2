import style from "./Avatar.module.scss";
import Image from "next/image";
import avatar from '../../public/juli.png'

export default function AvatarModule() {
    return (
        <div className={style.avatar}>
            <Image src={avatar}/>
            <h3>Юлия Иванова</h3>
        </div>
    )
}
