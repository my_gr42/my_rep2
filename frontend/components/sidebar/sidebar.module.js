import LogoModule from "./logo.module";
import style from "./SideBar.module.scss"
import AvatarModule from "./avatar.module";
import MenuModule from "./menu.module";

export default function SideBarModule() {
    return (
        <div className={style.sidebar}>
            <LogoModule/>
            <AvatarModule/>
            <MenuModule/>
        </div>
    )
}
