import style from './Menu.module.scss'
import Link from 'next/link'
import Icon from '../icon'

export default function MenuModule() {
    return (
        <div className={style.menu}>
            <ul>
                <li>
                    <Link href="/">
                        <div>
                            <Icon icon="map"/>
                            Главная
                        </div>
                    </Link>
                </li>
                <li>
                    <Link href="/reports">
                        <div>
                            <Icon icon="draft"/>
                            Отчеты
                        </div>
                    </Link>
                </li>
                <li>
                    <Link href="/calendar">
                        <div>
                            <Icon icon="calendar"/> Календарь
                        </div>
                    </Link>
                </li>
                <li>
                    <Link href="/settings">
                        <div>
                            <Icon icon="settings"/>
                        Настройки
                        </div>
                    </Link>
                </li>
            </ul>
        </div>
    )
}
