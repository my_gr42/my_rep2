import MainModule from "../components/dashboard/main.module"
import MainLayout from "../components/MainLayout";
import * as h3 from "h3-js";

const getWeight = (count, min, max) => {
    if (count > 0) {
        let op = Math.round((10 * (count - min)) / (max - min)) / 10;
        return op > 0.1 ? op : 0.1;
    } else return 0.1;
};

const groupPolygonsByOpacity = (polygons) => {
    const retMap = {}; // e.g {0.1: [hex1, hex2]}
    polygons.forEach(([pol, weight]) => {
        if (retMap['coordinates']) {
            retMap['coordinates'] = [...retMap[weight.toString()], pol];
        } else {
            retMap['coordinates'] = [pol];
        }
    });
    return retMap;
};

export default function Home({geoH3}) {
    return (
        <MainLayout>
            <MainModule geo={geoH3}/>
        </MainLayout>

    )
}

Home.getInitialProps = async () => {
    const resp = await fetch('http://localhost:3000/api/geo')
    const geo = await resp.json()
 console.log(geo['geo'])
    const h3s = geo['geo'].map((data) => {
        let hexagonMap = new Map()
        const hexCount = hexagonMap.get(data.h3);
        if (hexCount) {
            hexagonMap.set(data.h3, hexCount + 1);
        } else {
            hexagonMap.set(data.h3, 1);
        }

        Array.from(hexagonMap.keys()).forEach((key) => {
            const kring = h3.kRing(key);
            kring.forEach((hex) => {
                if (!hexagonMap.has(hex)) {
                    hexagonMap.set(hex, 0);
                }
            });
        });

        const [counts, keys] = [
            Array.from(hexagonMap.values()),
            Array.from(hexagonMap.keys())
        ];

        let max = Math.max(...counts);
        let min = Math.min(...counts);

        let hexagons = keys.map((key, i) => {
            let polygon = h3.h3ToGeoBoundary(key, false);
            return [polygon, getWeight(counts[i], min, max)];
        });

        return groupPolygonsByOpacity(hexagons);
    })

    const geoH3 = h3s.map((data, index) => {
        return {
            id: geo['geo'][index].index,
            h3: data,
            val: geo['geo'][index].val,
            color: geo['geo'][index].color
        }
    })
    return {
        geoH3
    }
}
