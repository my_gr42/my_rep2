import '../styles/globals.scss'
import '../public/fonts/style.css'
import {MyProvider} from "../context/state";

function AnalyticsDashboardApp({Component, pageProps}) {
    return (
        <MyProvider>
            <Component {...pageProps} />
        </MyProvider>)
}

export default AnalyticsDashboardApp
