#!/bin/bash

sudo docker rm cpf -f

sudo docker run --name cpf -t -d -p 8666:8666 \
     --network host \
     --volume="/home/user/work:/root/work" \
     --cap-add SYS_ADMIN  \
     --cap-add DAC_READ_SEARCH \
     cpf
 
# -e "LD_LIBRARY_PATH=/usr/local/cuda/extras/CUPTI/lib64:/usr/local/cuda/lib64:/usr/include/x64_64-linux-gnu:/usr/local/nvidia/lib:/usr/local/nvidia/lib64" \
    